<?php
/**
 * Created by PhpStorm.
 * User: arnaud
 * Date: 05/02/19
 * Time: 10:00
 */

namespace App\Repositories;

use App\User;

interface UserRepository extends ResourceRepository
{
    public function save(User $user, Array $inputs);

    public function store(Array $inputs);

    public function getById($id);

    public function update($id, Array $inputs);

    public function destroy($id);

    public function getPaginate($n);
}