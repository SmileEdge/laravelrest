<?php

namespace App\Repositories;

use App\User;

class UserRepositoryImpl extends ResourceRepositoryImpl
{

    protected $user;

    public function __construct(User $user)
    {
        $this->model = $user;
    }
}