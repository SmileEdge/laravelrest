<?php

namespace App\Repositories;

interface TagRepository
{

    public function store($post, $tags);

}