<?php

namespace App\Repositories;

use App\Post;

interface PostRepository
{

    public function __construct(Post $post);

    public function queryWithUserAndTags();

    public function getWithUserAndTagsPaginate($n);

    public function getWithUserAndTagsForTagPaginate($tag, $n);

    public function store($inputs);

    public function destroy($id);

}