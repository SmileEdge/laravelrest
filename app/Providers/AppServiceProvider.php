<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\UserRepository',
            'App\Repositories\UserRepositoryImpl'
        );

        $this->app->bind(
            'App\Repositories\PostRepository',
            'App\Repositories\PostRepositoryImpl'
        );

        $this->app->bind(
            'App\Repositories\TagRepository',
            'App\Repositories\TagRepositoryImpl'
        );
    }
}
